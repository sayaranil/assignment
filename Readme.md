Assignment

Building the assignment :
Import the project to your IDE as a Maven project.

Activating REST API service :
To activate REST API service just run the application class that is in the assignment project company provided,
and then wait for the success message.

Checking REST API service :
To check service you need to open a browser and call "http://localhost:8181", or you can use "Postman" for checking.

Running the assignment :
To run the assignment open the terminal, write "mvn clean test" and press "Enter".

Seeing the results :
You can see result in two ways : through terminal and through Allure test report framework.
To reach the allure test framework you need to write "mvn site" to the terminal and press "Enter" after that go through :
assignment folder > target > site > allure-maven-plugin and open "index.html" on "Mozilla Firefox".
