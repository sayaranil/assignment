package fraction.tests;

import fraction.model.Fraction;
import org.hamcrest.Matchers;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;
import utils.ApiUtil;
import utils.Month;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

/**
 * Created by anilsayar on 8/26/17.
 */
public class GetFractionsByProfileTest {

    @Test
    @Features("Service test")
    @Description("Checking the service for further tests.")
    @Severity(SeverityLevel.BLOCKER)
    public void makeSureThatServiceIsUp() {
        String baseURL = "http://localhost:8181/";
        given().when().get(baseURL).then().statusCode(200);
        get(baseURL + "fractions").then().statusCode(200);
    }

    @Test
    @Features("Tests about Fractions")
    @Description("Adding and reading fractions by profile name A.")
    @Severity(SeverityLevel.NORMAL)
    public void getFractionsForA() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionA()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionB()))
                .when().get("/fraction/{profileName}", "A")
                .then().statusCode(200);

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));
    }

    private List<Fraction> prepareTestDataForFractionA() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"A"));
        fractions.add(new Fraction(1d, Month.FEB,"A"));
        fractions.add(new Fraction(0d, Month.MAR,"A"));
        fractions.add(new Fraction(0d, Month.APR,"A"));
        fractions.add(new Fraction(0d, Month.MAY,"A"));
        fractions.add(new Fraction(0d, Month.JUN,"A"));
        fractions.add(new Fraction(0d, Month.JUL,"A"));
        fractions.add(new Fraction(0d, Month.AUG,"A"));
        fractions.add(new Fraction(0d, Month.SEP,"A"));
        fractions.add(new Fraction(0d, Month.OCT,"A"));
        fractions.add(new Fraction(0d, Month.NOV,"A"));
        fractions.add(new Fraction(0d, Month.DEC,"A"));
        return fractions;
    }

    @Test
    @Features("Tests about Fractions")
    @Description("Adding and reading fractions by profile name B.")
    @Severity(SeverityLevel.NORMAL)
    public void getFractionsForB() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionB()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionB()))
                .when().get("/fraction/{profileName}", "B")
                .then().statusCode(200);

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));
    }

        private List<Fraction> prepareTestDataForFractionB() {
            List<Fraction> fractions = new ArrayList<>();
            fractions.add(new Fraction(0d, Month.JAN,"B"));
            fractions.add(new Fraction(0d, Month.FEB,"B"));
            fractions.add(new Fraction(0d, Month.MAR,"B"));
            fractions.add(new Fraction(0d, Month.APR,"B"));
            fractions.add(new Fraction(0d, Month.MAY,"B"));
            fractions.add(new Fraction(0d, Month.JUN,"B"));
            fractions.add(new Fraction(0d, Month.JUL,"B"));
            fractions.add(new Fraction(1d, Month.AUG,"B"));
            fractions.add(new Fraction(0d, Month.SEP,"B"));
            fractions.add(new Fraction(0d, Month.OCT,"B"));
            fractions.add(new Fraction(0d, Month.NOV,"B"));
            fractions.add(new Fraction(0d, Month.DEC,"B"));
            return fractions;
        }
}
