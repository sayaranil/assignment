package fraction.model;

import utils.Month;

/**
 * Created by anilsayar on 8/26/17.
 */
public class MonthProfileKey {

    private final Month month;
    public String profile;

    public MonthProfileKey(Month month, String profile) {
        this.month = month;
        this.profile = profile;
    }

    public Month getMonth() {
        return month;
    }

    public String getProfile() {
        return profile;
    }
}
