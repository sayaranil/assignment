package fraction.model;

import utils.Month;

/**
 * Fraction
 */
public class Fraction {

    private final Double fraction;
    private final MonthProfileKey monthProfileKey;

    public Fraction(Double fraction, Month month, String profile) {
        this.fraction = fraction;
        this.monthProfileKey = new MonthProfileKey(month,profile);
    }

    public Double getFraction() {
        return fraction;
    }

    public MonthProfileKey getMonthProfileKey() {
        return monthProfileKey;
    }
}
