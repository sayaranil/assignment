package utils;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.specification.RequestSpecification;

/**
 * ApiUtil
 */
public class ApiUtil {

    private static final String API_URL = "http://localhost:8181/consumption/";

    public static RequestSpecification prepareSpecWithBody(Object o) {

        // Building request using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setContentType("application/json; charset=UTF-8");
        builder.setBaseUri(API_URL);

        builder.setBody(o);

        return builder.build();
    }

    public static RequestSpecification prepareSpec() {

        // Building request using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setContentType("application/json; charset=UTF-8");
        builder.setBaseUri(API_URL);

        return builder.build();
    }

}
