package meterReading.tests;

import fraction.model.Fraction;
import meterReading.model.MeterReading;
import org.hamcrest.Matchers;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;
import utils.ApiUtil;
import utils.Month;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

/**
 * Created by anilsayar on 8/27/17.
 */
public class MeterReadingsAddTest {

    @Test
    @Features("Service test")
    @Description("Checking the service for further tests.")
    @Severity(SeverityLevel.BLOCKER)
    public void makeSureThatServiceIsUp() {
        String baseURL = "http://localhost:8181/";
        given().when().get(baseURL).then().statusCode(200);
        get(baseURL + "fractions").then().statusCode(200);
    }

    @Test
    @Features("Tests about Meter Readings")
    @Description("Checking the meter reading for profile A.")
    @Severity(SeverityLevel.NORMAL)
    public void addMeterReadingForA() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A");

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "A");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionA()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingA()))
                .when().post("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

    }

    private List<Fraction> prepareTestDataForFractionA() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"A"));
        fractions.add(new Fraction(1d, Month.FEB,"A"));
        fractions.add(new Fraction(0d, Month.MAR,"A"));
        fractions.add(new Fraction(0d, Month.APR,"A"));
        fractions.add(new Fraction(0d, Month.MAY,"A"));
        fractions.add(new Fraction(0d, Month.JUN,"A"));
        fractions.add(new Fraction(0d, Month.JUL,"A"));
        fractions.add(new Fraction(0d, Month.AUG,"A"));
        fractions.add(new Fraction(0d, Month.SEP,"A"));
        fractions.add(new Fraction(0d, Month.OCT,"A"));
        fractions.add(new Fraction(0d, Month.NOV,"A"));
        fractions.add(new Fraction(0d, Month.DEC,"A"));
        return fractions;
    }

    private List<MeterReading> prepareTestDataForMeterReadingA() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"A",10));
        meterreading.add(new MeterReading(1, Month.FEB,"A",10));
        meterreading.add(new MeterReading(0, Month.MAR,"A",10));
        meterreading.add(new MeterReading(0, Month.APR,"A",10));
        meterreading.add(new MeterReading(0, Month.MAY,"A",10));
        meterreading.add(new MeterReading(0, Month.JUN,"A",10));
        meterreading.add(new MeterReading(0, Month.JUL,"A",10));
        meterreading.add(new MeterReading(0, Month.AUG,"A",10));
        meterreading.add(new MeterReading(0, Month.SEP,"A",10));
        meterreading.add(new MeterReading(0, Month.OCT,"A",10));
        meterreading.add(new MeterReading(0, Month.NOV,"A",10));
        meterreading.add(new MeterReading(0, Month.DEC,"A",10));
        return meterreading;
    }

    @Test
    @Features("Tests about Meter Readings")
    @Description("Checking the BAD_REQUEST for meter reading A. ")
    @Severity(SeverityLevel.CRITICAL)
    public void addMeterReadingForABadRequest() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A");

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "A");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionABadRequest()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingABadRequest()))
                .when().post("/meterreading")
                .then().statusCode(400).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

    }

    private List<Fraction> prepareTestDataForFractionABadRequest() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"A"));
        fractions.add(new Fraction(1d, Month.FEB,"A"));
        fractions.add(new Fraction(0d, Month.MAR,"A"));
        fractions.add(new Fraction(0d, Month.APR,"A"));
        fractions.add(new Fraction(0d, Month.MAY,"A"));
        fractions.add(new Fraction(0d, Month.JUN,"A"));
        fractions.add(new Fraction(0d, Month.JUL,"A"));
        fractions.add(new Fraction(0d, Month.AUG,"A"));
        fractions.add(new Fraction(0d, Month.SEP,"A"));
        fractions.add(new Fraction(0d, Month.OCT,"A"));
        fractions.add(new Fraction(0d, Month.NOV,"A"));
        fractions.add(new Fraction(0d, Month.DEC,"A"));
        return fractions;
    }

    private List<MeterReading> prepareTestDataForMeterReadingABadRequest() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"A",10));
        meterreading.add(new MeterReading(1, Month.FEB,"A",10));
        meterreading.add(new MeterReading(0, Month.MAR,"A",10));
        meterreading.add(new MeterReading(0, Month.APR,"A",10));
        meterreading.add(new MeterReading(0, Month.MAY,"A",10));
        meterreading.add(new MeterReading(0, Month.JUN,"A",10));
        meterreading.add(new MeterReading(0, Month.JUL,"A",10));
        meterreading.add(new MeterReading(0, Month.AUG,"A",10));
        meterreading.add(new MeterReading(0, Month.SEP,"A",10));
        meterreading.add(new MeterReading(0, Month.OCT,"A",10));
        meterreading.add(new MeterReading(0, Month.NOV,"A",10));
        meterreading.add(new MeterReading(0, Month.DEC,"A",10));
        return meterreading;
    }

    @Test
    @Features("Tests about Meter Readings")
    @Description("Checking the meter reading for profile B.")
    @Severity(SeverityLevel.NORMAL)
    public void addMeterReadingForB() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B");

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "B");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionB()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingB()))
                .when().post("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "B")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

    }

    private List<Fraction> prepareTestDataForFractionB() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"B"));
        fractions.add(new Fraction(1d, Month.FEB,"B"));
        fractions.add(new Fraction(0d, Month.MAR,"B"));
        fractions.add(new Fraction(0d, Month.APR,"B"));
        fractions.add(new Fraction(0d, Month.MAY,"B"));
        fractions.add(new Fraction(0d, Month.JUN,"B"));
        fractions.add(new Fraction(0d, Month.JUL,"B"));
        fractions.add(new Fraction(0d, Month.AUG,"B"));
        fractions.add(new Fraction(0d, Month.SEP,"B"));
        fractions.add(new Fraction(0d, Month.OCT,"B"));
        fractions.add(new Fraction(0d, Month.NOV,"B"));
        fractions.add(new Fraction(0d, Month.DEC,"B"));
        return fractions;
    }


    private List<MeterReading> prepareTestDataForMeterReadingB() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"B",10));
        meterreading.add(new MeterReading(0, Month.FEB,"B",10));
        meterreading.add(new MeterReading(0, Month.MAR,"B",10));
        meterreading.add(new MeterReading(0, Month.APR,"B",10));
        meterreading.add(new MeterReading(0, Month.MAY,"B",10));
        meterreading.add(new MeterReading(0, Month.JUN,"B",10));
        meterreading.add(new MeterReading(0, Month.JUL,"B",10));
        meterreading.add(new MeterReading(1, Month.AUG,"B",10));
        meterreading.add(new MeterReading(0, Month.SEP,"B",10));
        meterreading.add(new MeterReading(0, Month.OCT,"B",10));
        meterreading.add(new MeterReading(0, Month.NOV,"B",10));
        meterreading.add(new MeterReading(0, Month.DEC,"B",10));
        return meterreading;
    }

    @Test
    @Features("Tests about Meter Readings")
    @Description("Checking the BAD_REQUEST for meter reading A. ")
    @Severity(SeverityLevel.CRITICAL)
    public void addMeterReadingForBBadRequest() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B");

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "B");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionBBadRequest()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingBBadRequest()))
                .when().post("/meterreading")
                .then().statusCode(400).assertThat().body(Matchers.equalTo("BAD_REQUEST"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "B")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

    }

    private List<Fraction> prepareTestDataForFractionBBadRequest() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"B"));
        fractions.add(new Fraction(1d, Month.FEB,"B"));
        fractions.add(new Fraction(0d, Month.MAR,"B"));
        fractions.add(new Fraction(0d, Month.APR,"B"));
        fractions.add(new Fraction(0d, Month.MAY,"B"));
        fractions.add(new Fraction(0d, Month.JUN,"B"));
        fractions.add(new Fraction(0d, Month.JUL,"B"));
        fractions.add(new Fraction(0d, Month.AUG,"B"));
        fractions.add(new Fraction(0d, Month.SEP,"B"));
        fractions.add(new Fraction(0d, Month.OCT,"B"));
        fractions.add(new Fraction(0d, Month.NOV,"B"));
        fractions.add(new Fraction(0d, Month.DEC,"B"));
        return fractions;
    }

    private List<MeterReading> prepareTestDataForMeterReadingBBadRequest() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"B",10));
        meterreading.add(new MeterReading(1, Month.FEB,"B",10));
        meterreading.add(new MeterReading(0, Month.MAR,"B",10));
        meterreading.add(new MeterReading(0, Month.APR,"B",10));
        meterreading.add(new MeterReading(0, Month.MAY,"B",10));
        meterreading.add(new MeterReading(0, Month.JUN,"B",10));
        meterreading.add(new MeterReading(0, Month.JUL,"B",10));
        meterreading.add(new MeterReading(0, Month.AUG,"B",10));
        meterreading.add(new MeterReading(0, Month.SEP,"B",10));
        meterreading.add(new MeterReading(0, Month.OCT,"B",10));
        meterreading.add(new MeterReading(0, Month.NOV,"B",10));
        meterreading.add(new MeterReading(0, Month.DEC,"B",10));
        return meterreading;
    }
}
