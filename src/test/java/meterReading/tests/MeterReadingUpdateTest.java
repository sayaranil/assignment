package meterReading.tests;

import com.jayway.restassured.response.Response;
import fraction.model.Fraction;
import meterReading.model.MeterReading;
import org.hamcrest.Matchers;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;
import utils.ApiUtil;
import utils.Month;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by anilsayar on 8/27/17.
 */
public class MeterReadingUpdateTest {

    @Test
    @Features("Service test")
    @Description("Checking the service for further tests.")
    @Severity(SeverityLevel.BLOCKER)
    public void makeSureThatServiceIsUp() {
        String baseURL = "http://localhost:8181/";
        given().when().get(baseURL).then().statusCode(200);
        get(baseURL + "fractions").then().statusCode(200);
    }

    @Test
    @Features("Tests about Meter Readings")
    @Description("Checking the update for meter readings for profile A.")
    @Severity(SeverityLevel.NORMAL)
    public void updateMeterReadingForA() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A");

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "A");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionA()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingA()))
                .when().post("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingAUpdated()))
                .when().put("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));


        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

    }

    private List<Fraction> prepareTestDataForFractionA() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"A"));
        fractions.add(new Fraction(1d, Month.FEB,"A"));
        fractions.add(new Fraction(0d, Month.MAR,"A"));
        fractions.add(new Fraction(0d, Month.APR,"A"));
        fractions.add(new Fraction(0d, Month.MAY,"A"));
        fractions.add(new Fraction(0d, Month.JUN,"A"));
        fractions.add(new Fraction(0d, Month.JUL,"A"));
        fractions.add(new Fraction(0d, Month.AUG,"A"));
        fractions.add(new Fraction(0d, Month.SEP,"A"));
        fractions.add(new Fraction(0d, Month.OCT,"A"));
        fractions.add(new Fraction(0d, Month.NOV,"A"));
        fractions.add(new Fraction(0d, Month.DEC,"A"));
        return fractions;
    }

    private List<MeterReading> prepareTestDataForMeterReadingA() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"A",0));
        meterreading.add(new MeterReading(1, Month.FEB,"A",0));
        meterreading.add(new MeterReading(0, Month.MAR,"A",0));
        meterreading.add(new MeterReading(0, Month.APR,"A",0));
        meterreading.add(new MeterReading(0, Month.MAY,"A",0));
        meterreading.add(new MeterReading(0, Month.JUN,"A",0));
        meterreading.add(new MeterReading(0, Month.JUL,"A",0));
        meterreading.add(new MeterReading(0, Month.AUG,"A",0));
        meterreading.add(new MeterReading(0, Month.SEP,"A",0));
        meterreading.add(new MeterReading(0, Month.OCT,"A",0));
        meterreading.add(new MeterReading(0, Month.NOV,"A",0));
        meterreading.add(new MeterReading(0, Month.DEC,"A",0));
        return meterreading;
    }

    private List<MeterReading> prepareTestDataForMeterReadingAUpdated() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"A",1));
        meterreading.add(new MeterReading(1, Month.FEB,"A",1));
        meterreading.add(new MeterReading(0, Month.MAR,"A",1));
        meterreading.add(new MeterReading(0, Month.APR,"A",1));
        meterreading.add(new MeterReading(0, Month.MAY,"A",1));
        meterreading.add(new MeterReading(0, Month.JUN,"A",1));
        meterreading.add(new MeterReading(0, Month.JUL,"A",1));
        meterreading.add(new MeterReading(0, Month.AUG,"A",1));
        meterreading.add(new MeterReading(0, Month.SEP,"A",1));
        meterreading.add(new MeterReading(0, Month.OCT,"A",1));
        meterreading.add(new MeterReading(0, Month.NOV,"A",1));
        meterreading.add(new MeterReading(0, Month.DEC,"A",1));
        return meterreading;
    }

    @Test
    @Features("Tests about Meter Readings")
    @Description("Checking the update for meter readings for profile B.")
    @Severity(SeverityLevel.NORMAL)
    public void updateMeterReadingForB() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B");

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "B");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionB()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingB()))
                .when().post("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingBUpdated()))
                .when().put("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "B")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

    }

    private List<Fraction> prepareTestDataForFractionB() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"A"));
        fractions.add(new Fraction(1d, Month.FEB,"A"));
        fractions.add(new Fraction(0d, Month.MAR,"A"));
        fractions.add(new Fraction(0d, Month.APR,"A"));
        fractions.add(new Fraction(0d, Month.MAY,"A"));
        fractions.add(new Fraction(0d, Month.JUN,"A"));
        fractions.add(new Fraction(0d, Month.JUL,"A"));
        fractions.add(new Fraction(0d, Month.AUG,"A"));
        fractions.add(new Fraction(0d, Month.SEP,"A"));
        fractions.add(new Fraction(0d, Month.OCT,"A"));
        fractions.add(new Fraction(0d, Month.NOV,"A"));
        fractions.add(new Fraction(0d, Month.DEC,"A"));
        return fractions;
    }

    private List<MeterReading> prepareTestDataForMeterReadingB() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"A",0));
        meterreading.add(new MeterReading(1, Month.FEB,"A",0));
        meterreading.add(new MeterReading(0, Month.MAR,"A",0));
        meterreading.add(new MeterReading(0, Month.APR,"A",0));
        meterreading.add(new MeterReading(0, Month.MAY,"A",0));
        meterreading.add(new MeterReading(0, Month.JUN,"A",0));
        meterreading.add(new MeterReading(0, Month.JUL,"A",0));
        meterreading.add(new MeterReading(0, Month.AUG,"A",0));
        meterreading.add(new MeterReading(0, Month.SEP,"A",0));
        meterreading.add(new MeterReading(0, Month.OCT,"A",0));
        meterreading.add(new MeterReading(0, Month.NOV,"A",0));
        meterreading.add(new MeterReading(0, Month.DEC,"A",0));
        return meterreading;
    }

    private List<MeterReading> prepareTestDataForMeterReadingBUpdated() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"A",1));
        meterreading.add(new MeterReading(1, Month.FEB,"A",1));
        meterreading.add(new MeterReading(0, Month.MAR,"A",1));
        meterreading.add(new MeterReading(0, Month.APR,"A",1));
        meterreading.add(new MeterReading(0, Month.MAY,"A",1));
        meterreading.add(new MeterReading(0, Month.JUN,"A",1));
        meterreading.add(new MeterReading(0, Month.JUL,"A",1));
        meterreading.add(new MeterReading(0, Month.AUG,"A",1));
        meterreading.add(new MeterReading(0, Month.SEP,"A",1));
        meterreading.add(new MeterReading(0, Month.OCT,"A",1));
        meterreading.add(new MeterReading(0, Month.NOV,"A",1));
        meterreading.add(new MeterReading(0, Month.DEC,"A",1));
        return meterreading;
    }

    @Test
    @Features("Tests about Meter Readings")
    @Description("Checking the fail update for meter readings for profile A.")
    @Severity(SeverityLevel.CRITICAL)
    public void updateMeterReadingForFailA() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A");

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "A");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionFailA()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingFailA()))
                .when().post("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingFailAUpdated()))
                .when().put("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        Response response = given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingB()))
                .when().get("/meterreading/{profileName}", "A")
                .thenReturn();

        assertThat(response.getStatusCode(), equalTo(200));

        List<MeterReading> meterReadings = Arrays.asList(response.as(MeterReading[].class));
        assertThat(meterReadings.size(), equalTo(12));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));


    }

    private List<Fraction> prepareTestDataForFractionFailA() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"A"));
        fractions.add(new Fraction(1d, Month.FEB,"A"));
        fractions.add(new Fraction(0d, Month.MAR,"A"));
        fractions.add(new Fraction(0d, Month.APR,"A"));
        fractions.add(new Fraction(0d, Month.MAY,"A"));
        fractions.add(new Fraction(0d, Month.JUN,"A"));
        fractions.add(new Fraction(0d, Month.JUL,"A"));
        fractions.add(new Fraction(0d, Month.AUG,"A"));
        fractions.add(new Fraction(0d, Month.SEP,"A"));
        fractions.add(new Fraction(0d, Month.OCT,"A"));
        fractions.add(new Fraction(0d, Month.NOV,"A"));
        fractions.add(new Fraction(0d, Month.DEC,"A"));
        return fractions;
    }

    private List<MeterReading> prepareTestDataForMeterReadingFailA() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"A",0));
        meterreading.add(new MeterReading(1, Month.FEB,"A",0));
        meterreading.add(new MeterReading(0, Month.MAR,"A",0));
        meterreading.add(new MeterReading(0, Month.APR,"A",0));
        meterreading.add(new MeterReading(0, Month.MAY,"A",0));
        meterreading.add(new MeterReading(0, Month.JUN,"A",0));
        meterreading.add(new MeterReading(0, Month.JUL,"A",0));
        meterreading.add(new MeterReading(0, Month.AUG,"A",0));
        meterreading.add(new MeterReading(0, Month.SEP,"A",0));
        meterreading.add(new MeterReading(0, Month.OCT,"A",0));
        meterreading.add(new MeterReading(0, Month.NOV,"A",0));
        meterreading.add(new MeterReading(0, Month.DEC,"A",0));
        return meterreading;
    }

    private List<MeterReading> prepareTestDataForMeterReadingFailAUpdated() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"A",1));
        meterreading.add(new MeterReading(1, Month.FEB,"A",1));
        meterreading.add(new MeterReading(0, Month.MAR,"A",1));
        meterreading.add(new MeterReading(0, Month.APR,"A",1));
        meterreading.add(new MeterReading(0, Month.MAY,"A",1));
        meterreading.add(new MeterReading(0, Month.JUN,"A",1));
        meterreading.add(new MeterReading(0, Month.JUL,"A",1));
        meterreading.add(new MeterReading(0, Month.AUG,"A",1));
        meterreading.add(new MeterReading(0, Month.SEP,"A",1));
        meterreading.add(new MeterReading(0, Month.OCT,"A",1));
        meterreading.add(new MeterReading(0, Month.NOV,"A",1));
        meterreading.add(new MeterReading(0, Month.DEC,"A",1));
        return meterreading;
    }

    @Test
    @Features("Tests about Meter Readings")
    @Description("Checking the fail update for meter readings for profile B.")
    @Severity(SeverityLevel.CRITICAL)
    public void updateMeterReadingForFailB() {

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "B");

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/meterreading/{profileName}", "B");

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForFractionFailB()))
                .when().post("/fraction")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingFailB()))
                .when().post("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingFailBUpdated()))
                .when().put("/meterreading")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

        Response response = given().spec(ApiUtil.prepareSpecWithBody(prepareTestDataForMeterReadingB()))
                .when().get("/meterreading/{profileName}", "B")
                .thenReturn();

        assertThat(response.getStatusCode(), equalTo(200));

        List<MeterReading> meterReadings = Arrays.asList(response.as(MeterReading[].class));
        assertThat(meterReadings.size(), equalTo(12));

        given().spec(ApiUtil.prepareSpec())
                .when().delete("/fraction/{profileName}", "A")
                .then().statusCode(200).assertThat().body(Matchers.equalTo("Success"));

    }

    private List<Fraction> prepareTestDataForFractionFailB() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(new Fraction(0d, Month.JAN,"B"));
        fractions.add(new Fraction(1d, Month.FEB,"B"));
        fractions.add(new Fraction(0d, Month.MAR,"B"));
        fractions.add(new Fraction(0d, Month.APR,"B"));
        fractions.add(new Fraction(0d, Month.MAY,"B"));
        fractions.add(new Fraction(0d, Month.JUN,"B"));
        fractions.add(new Fraction(0d, Month.JUL,"B"));
        fractions.add(new Fraction(0d, Month.AUG,"B"));
        fractions.add(new Fraction(0d, Month.SEP,"B"));
        fractions.add(new Fraction(0d, Month.OCT,"B"));
        fractions.add(new Fraction(0d, Month.NOV,"B"));
        fractions.add(new Fraction(0d, Month.DEC,"B"));
        return fractions;
    }

    private List<MeterReading> prepareTestDataForMeterReadingFailB() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"B",0));
        meterreading.add(new MeterReading(1, Month.FEB,"B",0));
        meterreading.add(new MeterReading(0, Month.MAR,"B",0));
        meterreading.add(new MeterReading(0, Month.APR,"B",0));
        meterreading.add(new MeterReading(0, Month.MAY,"B",0));
        meterreading.add(new MeterReading(0, Month.JUN,"B",0));
        meterreading.add(new MeterReading(0, Month.JUL,"B",0));
        meterreading.add(new MeterReading(0, Month.AUG,"B",0));
        meterreading.add(new MeterReading(0, Month.SEP,"B",0));
        meterreading.add(new MeterReading(0, Month.OCT,"B",0));
        meterreading.add(new MeterReading(0, Month.NOV,"B",0));
        meterreading.add(new MeterReading(0, Month.DEC,"B",0));
        return meterreading;
    }

    private List<MeterReading> prepareTestDataForMeterReadingFailBUpdated() {
        List<MeterReading> meterreading = new ArrayList<>();
        meterreading.add(new MeterReading(0, Month.JAN,"B",1));
        meterreading.add(new MeterReading(1, Month.FEB,"B",1));
        meterreading.add(new MeterReading(0, Month.MAR,"B",1));
        meterreading.add(new MeterReading(0, Month.APR,"B",1));
        meterreading.add(new MeterReading(0, Month.MAY,"B",1));
        meterreading.add(new MeterReading(0, Month.JUN,"B",1));
        meterreading.add(new MeterReading(0, Month.JUL,"B",1));
        meterreading.add(new MeterReading(0, Month.AUG,"B",1));
        meterreading.add(new MeterReading(0, Month.SEP,"B",1));
        meterreading.add(new MeterReading(0, Month.OCT,"B",1));
        meterreading.add(new MeterReading(0, Month.NOV,"B",1));
        meterreading.add(new MeterReading(0, Month.DEC,"B",1));
        return meterreading;
    }

}
