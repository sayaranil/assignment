package meterReading.model;

import fraction.model.MonthProfileKey;
import utils.Month;

/**
 * Created by anilsayar on 8/27/17.
 */
public class MeterReading {

    private final int connectionId;
    private final MonthProfileKey monthProfileKey;
    private int meterReading;

    public MeterReading(int connectionId, Month month, String profile, int meterReading) {
        this.connectionId = connectionId;
        this.monthProfileKey = new MonthProfileKey(month,profile);
        this.meterReading = meterReading;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public MonthProfileKey getMonthProfileKey() {
        return monthProfileKey;
    }

    public int getMeterReading() {
        return meterReading;
    }

    public void setMeterReading(int meterReading) {
        this.meterReading = meterReading;
    }
}
